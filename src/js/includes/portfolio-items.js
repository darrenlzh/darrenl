export const ITEMS = [
  {
    name: 'Weather in Angular 2',
    desc: 'A weather forecast app built with Angular 2, Node and Express.',
    cover: 'img/weather-cover.png',
    main: 'img/student-success.png',
    link: 'http://darrenl.im/weather'
  },
  {
    name: 'Student Success',
    desc: 'University at Buffalo Student Success site. Built with Sass, Pug, vanilla JavaScript and Gulp tools to automate workflow.',
    cover: 'img/student-success-cover.png',
    main: 'img/student-success.png',
    link: 'http://studentsuccess.buffalo.edu'

  },
  {
    name: 'Weather App',
    desc: 'Weather forecast app built with AngularJS and apixu API.',
    cover: 'img/weather-app-cover.png',
    main: 'img/student-success.png',
    link: 'http://darrenl.im/weather-app'
  },
  {
    name: 'Academic Integrity',
    desc: 'University at Buffalo Academic Integrity site. Built with Sass & Pug with Gulp tools.',
    cover: 'img/academic-integrity-cover.png',
    main: 'img/student-success.png',
    link: 'http://academicintegrity.buffalo.edu'
  },
  {
    name: 'Marquise Patisserie & Chocolaterie',
    desc: 'Marquise Patisserie & Chocolaterie online store front.',
    cover: 'img/marquise-cover.png',
    main: 'img/student-success.png',
    link: '#'
  },
  {
    name: 'MozzCato Food Trading Co.',
    desc: 'MozzCato online store front.',
    cover: 'img/mozzcato-cover.png',
    main: 'img/student-success.png',
    link: '#'
  },
  {
    name: 'iLearns Kinesthetic Learning App',
    desc: 'Classroom learning with technology for elementary students',
    cover: 'img/ilearns-cover.png',
    main: 'img/student-success.png',
    link: '#'
  }
]
